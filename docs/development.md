# Development

## Requirements

- Node v16.15.x
- Yarn 1.22.x

## Technologies

- React

## Local install

### Dependencies

Install the project dependencies
```
yarn
```

## Run

### Webapp

```bash
yarn start
```

## Links to libs and other docs

- Paas Docs: https://paas.docs.cern.ch/