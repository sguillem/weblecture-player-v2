# CERN Video Player (Powered with Paella Player 7)

<p align="center"><img src="video.png" alt="Video logo" height="200"></p>

This application integrates Paella Player 7 to play CERN's lectures. It has integration with Openid for the protected ones.

## Requirements

- Node 16.15.x
- Yarn 1.22.17

Please, check the package.json file to know more about the project dependencies.

## Development

```bash
yarn
```

```bash
yarn start
```

Then open `http://localhost:3000` in your browser.

> ⚠️ Please be aware that protected lectures may not work due to the Openid cookies and authentication. If you need to test protected lectures, please consider creating a OKD4 project ([Paas CERN](https://paas.docs.cern.ch)) and add the new hostname to the allowed hostnames in the ocmedia servers.


## Query parameters

- `id`: Id of the lecture (12345, 12345c1)
- `year`: Year of the lecture (2020, 2021, etc)
- `qa`: Whether or not is a qa request.
- `local`: Whether it should only load local assets
- `embed=true`: Will disable the SSO. Useful for public embeds.

### time - Time seek parameters

`time`: A comma separated list of times that will allow the user to jump to different times of the video, displaying arrows in the playback bar.

#### time examples

- `time=90s`: Will seek to 1 minute 30 secs. Video will start on that time.
- `time=90s,12m30s`: Will seek to 2 different times. If the time is in `hms` format, it will be converted interally to seconds.
- `time=1h30m40s`: Same as first example.

### Examples

Loading a resource from the qa media server:

```
https://weblecture-player.web.cern.ch/?id=1122970c1&year=2022&qa=true
```

Loading a local resource:

- As of 24/06/2022 available resources are located in `public/manifests` folder and are `cern` or `test` (name of the folders):

```
http://localhost:3000/?id=test&local=true
http://localhost:3000/?id=cern&local=true
```

Loading a resource from the production media server

```
https://weblecture-player.web.cern.ch/?id=1122970c1&year=2022
```

## Media examples


| URL | Restrictions | Year | Subtitles |
|-----|------| ---- | --- |
| https://weblecture-player.web.cern.ch/?id=CERN-VIDEO-C-12-A-B&year=1982 | Public | 1982 | No |
| https://weblecture-player.web.cern.ch/?id=CERN-VIDEO-C-144-A&year=1993 | Public | 1993 | No |
| https://weblecture-player.web.cern.ch/?id=CERN-VIDEO-C-307&year=1997 | Public | 1997 | No |
| https://weblecture-player.web.cern.ch/?id=CERN-VIDEO-C-444&year=1999 | Public | 1999 | No |
| https://weblecture-player.web.cern.ch/?id=197461&year=2012 (Higgs Discovery) | Public | 2012 | No |
| https://weblecture-player.web.cern.ch/?year=2022&id=932902c44 | Public | 2022 | No |
| https://weblecture-player.web.cern.ch/?id=932902c49&year=2022 | Public | 2022 | No |
| https://weblecture-player.web.cern.ch/?id=932902c48&year=2022 | Public | 2022 | No |
| https://weblecture-player.web.cern.ch/?id=1135177c9&year=2022 | Public | 2022 | Yes |
| https://weblecture-player.web.cern.ch/?id=1106493&year=2022 | Restricted | 2022 | Yes |
| https://weblecture-player.web.cern.ch/?id=1122970c25&year=2022 | Restricted | 2022 | Yes |
| https://weblecture-player.web.cern.ch/?year=2022&id=1122970c1 | Restricted | 2022 | Yes |
## Learn More

- Paella Player Core: [Github](https://github.com/polimediaupv/paella-core)
- Paella Basic Plugins: [Github](https://github.com/polimediaupv/paella-basic-plugins)
- Paella Slide Plugins: [Github](https://github.com/polimediaupv/paella-slide-plugins)
- You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
- To learn React, check out the [React documentation](https://reactjs.org/).

## Attributions

- <a href="https://www.flaticon.com/free-icons/video" title="video icons">Video icon created by Freepik - Flaticon</a>