export const PACKAGE_VERSION = "2.4.0";
export const PAELLA_CORE_VERSION = "1.41.0";
export const PAELLA_BASIC_PLUGINS_VERSION = "1.23.0";
