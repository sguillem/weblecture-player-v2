import React, { useEffect } from "react";
import { Button, Center, Space, Stack } from "@mantine/core";
import Player from "components/player/Player";
import useCheckResources from "hooks/use-check-resources/use-check-resources";
import useDataJson from "hooks/use-data-json/use-data-json";
import AuthService from "services/auth-service";

export default function AppContent() {
  const { previewUrl } = useDataJson();
  const { resourcesRequireAuth, resourcesCheckComplete, resourcesAccesible } =
    useCheckResources(previewUrl);

  useEffect(() => {
    console.log(
      `Resources:
- Require auth: ${resourcesRequireAuth}
- Check complete: ${resourcesCheckComplete}
- Are accesible: ${resourcesAccesible}`,
    );
    if (
      resourcesRequireAuth &&
      resourcesCheckComplete &&
      !AuthService.isLoggedIn()
    ) {
      console.log("Authenticating user...");
      AuthService.doLogin({
        redirectUri: encodeURI(window.location.href),
      });
    }
  }, [resourcesRequireAuth, resourcesCheckComplete, resourcesAccesible]);

  if (resourcesAccesible || AuthService.isLoggedIn()) {
    return <Player />;
  }
  return (
    <Center style={{ width: "100%", height: "100vh" }}>
      <Stack align="center">
        <div>
          This lecture is restricted. Click the following button to access it.
        </div>
        <Space h="xs" />
        <div>
          <Button
            variant="subtle"
            onClick={() =>
              AuthService.doLogin({
                redirectUri: encodeURI(window.location.href),
              })
            }
          >
            Login and access
          </Button>
        </div>
      </Stack>
    </Center>
  );
}
