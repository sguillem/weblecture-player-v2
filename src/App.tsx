import React from "react";
import { LoadingOverlay } from "@mantine/core";
import "./App.css";
import AppContent from "components/AppContent/AppContent";

interface Props {
  initialized: boolean;
}

function App({ initialized }: Props) {
  return (
    <div className="container">
      {initialized ? <AppContent /> : <LoadingOverlay visible />}
    </div>
  );
}

export default App;
