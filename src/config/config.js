//  This is the config file to implement paella player on cern's network

const dev = {
  basicPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  subtitlesPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  webcastWebsiteUrl: "https://webcast-test.web.cern.ch",
  streamingUrl: "https://wowza.cern.ch",
  cdnUrl: "https://cern2.vo.llnwd.net/e1",
  ocmediaBaseUrl: "https://ocmedia-{SERVER}.cern.ch",
  KEYCLOAK: {
    CLIENT_ID: process.env.REACT_APP_OIDC_CLIENT_ID
      ? process.env.REACT_APP_OIDC_CLIENT_ID
      : "mbp_rene",
  },
  allowedReferers: [".cern.ch", ".cern", "127.0.0.1"],
};

const prod = {
  basicPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  subtitlesPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  webcastWebsiteUrl: "https://webcast.web.cern.ch",
  streamingUrl: "https://wowza.cern.ch",
  cdnUrl: "https://cern2.vo.llnwd.net/e1",
  ocmediaBaseUrl: "https://ocmedia-{SERVER}.cern.ch",
  KEYCLOAK: {
    CLIENT_ID: process.env.REACT_APP_OIDC_CLIENT_ID
      ? process.env.REACT_APP_OIDC_CLIENT_ID
      : "mbp_rene",
  },
  allowedReferers: [".cern.ch", ".cern"],
};

const test = {
  basicPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  subtitlesPath:
    "https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/",
  webcastWebsiteUrl: "https://webcast.web.cern.ch",
  streamingUrl: "https://wowza.cern.ch",
  cdnUrl: "https://cern2.vo.llnwd.net/e1",
  ocmediaBaseUrl: "https://ocmedia-{SERVER}.cern.ch",
  KEYCLOAK: {
    CLIENT_ID: process.env.REACT_APP_OIDC_CLIENT_ID
      ? process.env.REACT_APP_OIDC_CLIENT_ID
      : "mbp_rene",
  },
  allowedReferers: [".cern.ch", ".cern"],
};
// https://wowza.cern.ch/livehd/smil:863086_camera_all.smil/playlist.m3u8
// https://wowza.cern.ch/livehd/smil:863086_slides_all.smil/playlist.m3u8
let tempConfig = dev;

if (process.env.NODE_ENV === "production") {
  tempConfig = prod;
}

if (process.env.NODE_ENV === "test") {
  tempConfig = test;
}
const config = tempConfig;

export default config;
