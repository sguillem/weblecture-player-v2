import { utils } from "paella-core";
import cernConfig from "../config/config";

const { getUrlParameter } = utils;

/**
 * Generate a manifest (data.json) for stream using the provided title, camera and slides source.
 *
 * @param {string} title The title of the stream
 * @param {string} cameraSrc URL of the camera's stream (presenter)
 * @param {string} slidesSrc URL of the slides's stream (presentation)
 * @returns
 */
export function createLiveCameraSlidesManifest(title, cameraSrc, slidesSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}",
      "preview": "/images/preview.jpeg"
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${cameraSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presenter",
        "role": "mainAudio"
      },
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${slidesSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presentation"
      }
    ]
  }`;
  return jsonContent;
}

/**
 * Generate a manifest (data.json) for stream using the provided title, camera source.
 *
 * @param {string} title The title of the stream
 * @param {string} cameraSrc URL of the camera's stream (presenter)
 * @returns
 */
export function createLiveCameraManifest(title, cameraSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}"
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${cameraSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presenter",
        "role": "mainAudio"
      }
    ]
  }`;
  return jsonContent;
}

/**
 * Generate a manifest (data.json) for stream using the provided title and slides source.
 *
 * @param {string} title The title of the stream
 * @param {string} slidesSrc URL of the slides's stream (presentation)
 * @returns
 */
export function createLiveSlidesManifest(title, slidesSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}"
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${slidesSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presentation",
        "role": "mainAudio"
      }
    ]
  }`;
  return jsonContent;
}

export function getManifestUrlFunction(repoUrl, videoId) {
  const serverBefore2021 = "apo"; // <= 2020
  const serverAfter2021 = "bakony"; // > 2020
  const serverForQa = "montblanc"; // QA

  // origin, test
  const year = getUrlParameter("year");
  const isQa = getUrlParameter("qa");
  const isLocal = getUrlParameter("local");

  if (isLocal === "true") {
    return `/repository/${videoId}`;
  }

  // CEPH FS
  let server = serverAfter2021;
  // https://ocmedia-bakony.cern.ch/2020/856696c147/data.json
  if (Number(year) <= 2020) {
    server = serverBefore2021;
  }
  if (isQa === "true") {
    server = serverForQa;
  }

  const newBaseUrl = cernConfig.ocmediaBaseUrl.replace("{SERVER}", server);
  const manifestUrl = `${newBaseUrl}/${year}/${videoId}`;
  console.log("Generating manifest url ... OK");
  console.log(
    `Manifest url: ${manifestUrl} (local: ${isLocal} qa: ${isQa} year: ${year})`,
  );
  return manifestUrl;
}

export function disableCorsCredentialsForPlugin(pluginName, jsonConfig) {
  const newConfig = jsonConfig;
  newConfig.plugins[pluginName].corsConfig.withCredentials = false;
  newConfig.plugins[pluginName].corsConfig.requestHeaders[
    "Access-Control-Allow-Credentials"
  ] = false;
  return newConfig;
}
