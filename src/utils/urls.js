import config from "config/config";

export function getUrlParameter(name) {
  // Optional: implement this using a fallback to support IE11
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.has(name) ? urlParams.get(name) : null;
}

export function getCurrentURL() {
  return window.location.href;
}

export function getCurrentHostname() {
  return window.location.hostname;
}

export function getParentHostname() {
  console.log("document.referrer", document.referrer);
  return document.referrer;
}

export function isRefererAllowed(referer) {
  if (process.env.NODE_ENV !== "production") {
    if (referer === "localhost") {
      return true;
    }
    if (referer === "localhost:3000") {
      return true;
    }
  }
  // Iterate all the referers in the config and verify that the end of the string matches any of them
  const { allowedReferers } = config;
  if (allowedReferers) {
    for (let i = 0; i < allowedReferers.length; i += 1) {
      const allowedReferer = allowedReferers[i];
      if (referer.endsWith(allowedReferer)) {
        console.log(`Referer ${referer} is allowed`);
        return true;
      }
    }
  }
  return false;
}

export default getUrlParameter;
