import Keycloak from "keycloak-js";
import config from "./config/config";
// Setup Keycloak instance as needed
// Pass initialization options as required or leave blank to load from 'keycloak.json'
const keycloak = new Keycloak({
  url: "https://auth.cern.ch/auth",
  // url: "https://keycloak-qa.cern.ch/auth",
  realm: "cern",
  clientId: config.KEYCLOAK.CLIENT_ID,
  onLoad: "check-sso",
});

export default keycloak;
