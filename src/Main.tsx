import React, { useEffect } from "react";
import App from "./App";
import AuthService from "services/auth-service";
import { getParentHostname, isRefererAllowed } from "utils/urls";

export default function Main() {
  const [initialized, setInitialized] = React.useState(false);

  useEffect(() => {
    const onLoadCallback = () => {
      console.log("Keycloak initialized");
      setInitialized(true);
    };
    const referer = getParentHostname();

    if (
      !isRefererAllowed(window.location.hostname) &&
      !isRefererAllowed(referer)
    ) {
      setInitialized(true);
    } else {
      console.log("Initializing Keycloak...");
      AuthService.initKeycloak(onLoadCallback);
    }
  }, []);

  return <App initialized={initialized} />;
}
