import { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { IManifest } from "types/manifest";
import { getManifestUrlFunction } from "utils/paella-manifests";
import { getUrlParameter } from "utils/urls";

export default function useDataJson() {
  const [previewUrl, setPreviewUrl] = useState<string | undefined>(undefined);
  const [smilUrl, setSmilUrl] = useState<string | undefined>(undefined);
  const [dataJson, setDataJson] = useState<IManifest | undefined>(undefined);

  const getDataJson = useCallback(async () => {
    const videoId = getUrlParameter("id");
    const manifestUrl = getManifestUrlFunction("", videoId);
    try {
      const { data, status } = await axios.get<IManifest>(
        `${manifestUrl}/data.json`,
      );
      const responseJson = data;

      if (status === 200 && responseJson) {
        setDataJson(responseJson);
        console.log(`Fetching data.json... OK`);
      } else {
        console.log(`Fetching data.json... ERROR`);
        setDataJson(undefined);
      }
      setSmilUrl(`${manifestUrl}/${videoId}.smil`);
    } catch (error) {
      console.log(`Fetching data.json... ERROR`);
      setDataJson(undefined);
    }
  }, [setDataJson]);

  useEffect(() => {
    getDataJson();
  }, [getDataJson]);

  useEffect(() => {
    if (dataJson) {
      setPreviewUrl(dataJson.metadata.preview);
    }
  }, [dataJson]);

  return { dataJson, previewUrl, smilUrl };
}
