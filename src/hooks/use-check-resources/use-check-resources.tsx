import { useState, useEffect, useCallback } from "react";
import { getUrlParameter } from "utils/urls";

export default function useHasAccess(previewUrl: string | undefined) {
  const [resourcesRequireAuth, setResourcesRequireAuth] = useState(false);
  const [resourcesCheckComplete, setResourcesCheckComplete] = useState(false);
  const [resourcesAccesible, setResourcesAccesible] = useState(false);

  const checkAccess = useCallback(async () => {
    if (previewUrl) {
      const isLocal = getUrlParameter("local");
      const withCredentials = isLocal !== "true";
      console.log(`Get preview withCredentials: ${withCredentials}`);
      try {
        const response = await fetch(`${previewUrl}?not-cached`, {
          credentials: "include",
        });

        if (response.ok) {
          setResourcesAccesible(true);
          console.log(`Resources are accesible... OK`);
        } else {
          const message = `Bad response from server: ${response.status}`;
          console.log(message);
          if (response.status >= 301 && response.status < 600) {
            setResourcesAccesible(false);
            console.log(`Resources are accesible... NO`);
            throw new Error(message);
          }
        }
      } catch (error) {
        console.log(`Cannot access the preview. Restricted: ${error}`);
        setResourcesAccesible(false);
        setResourcesRequireAuth(true);
      }
      setResourcesCheckComplete(true);
      console.log(`Resource access check... COMPLETE`);
    }
  }, [previewUrl]);

  useEffect(() => {
    if (!resourcesCheckComplete) {
      checkAccess();
    }
  }, [resourcesCheckComplete, checkAccess]);

  return { resourcesRequireAuth, resourcesCheckComplete, resourcesAccesible };
}
