const PACKAGE_VERSION = require("../package.json").version;
const PAELLA_CORE_VERSION =
  require("../package.json").dependencies["paella-core"];
const PAELLA_BASIC_PLUGINS_VERSION =
  require("../package.json").dependencies["paella-basic-plugins"];

console.log(`export const PACKAGE_VERSION = "${PACKAGE_VERSION}";`);
console.log(`export const PAELLA_CORE_VERSION = "${PAELLA_CORE_VERSION}";`);
console.log(
  `export const PAELLA_BASIC_PLUGINS_VERSION = "${PAELLA_BASIC_PLUGINS_VERSION}";`,
);

console.error("package.json version:", PACKAGE_VERSION);
